--[SECTION] Inserting/Creating records 
INSERT INTO artists (name) VALUES ('Psy');
INSERT INTO artists (name) VALUES ('Linkin Park');
INSERT INTO artists (name) VALUES ('Backstreet Boys');

INSERT INTO albums (album_title, date_released, artist_id) VALUES (
  'Psy 6', 
  '2012-2-1', 
  1
);
INSERT INTO albums (album_title, date_released, artist_id) VALUES (
  'Meteora', 
  '2003-3-25', 
  2
);
INSERT INTO albums (album_title, date_released, artist_id) VALUES (
  'Millenium', 
  '1999-5-18', 
  3
);

INSERT INTO songs (song_name, length, genre, album_id) VALUES (
  'Gangnam Style', 
  253, 
  'K-Pop', 
  1
);

INSERT INTO songs (song_name, length, genre, album_id) VALUES (
  'Numb', 
  306, 
  'Rock', 
  2
);

INSERT INTO songs (song_name, length, genre, album_id) VALUES (
  'Lying from You', 
  255, 
  'Rock', 
  2
);

INSERT INTO songs (song_name, length, genre, album_id) VALUES (
  'I Want It That Way', 
  333, 
  'Pop', 
  3
);

--[SECTION] Selecting/retrieving records

-- display all information about all albums
SELECT * FROM albums; 

-- display only the title of all songs
SELECT song_name FROM songs; 

-- display only titles and genres from all songs 
SELECT song_name, genre FROM songs; 

-- display the title of all pop songs 
SELECT song_name FROM songs WHERE genre = 'Pop';

-- display the title and length of rock songs more than 3 mins long 
SELECT song_name, length FROM songs WHERE length > 259 AND genre = 'Rock';

-- The OR keyword can also be used to further modify SELECT conditions 

-- [SECTION] Updating records
 
UPDATE songs SET length = 240 WHERE song_name = 'Numb';
-- if there are multiple songs are named numb then all of them will update, better to use id instead
UPDATE songs SET length = 240; 
-- removing the where clause will update all the rows/records in the table 
UPDATE songs SET length = 240 WHERE id = 2;

-- [SECTION] Deleting records 

DELETE FROM songs WHERE genre = 'K-Pop';
-- deleting records doesn't affect the subsequent ids 
-- without the where clause all songs will be deleted 
-- ** removing the where clause will affect all rows/records 